import {Router} from "express";
import {ApiRoute} from "@src/routes/api";

export const BaseRouter = Router();

BaseRouter
  .get("/", (req, res) => res.json({success: true}))
  .use("/api", ApiRoute);
