import { Router } from "express";
import { AccountRoute } from "@src/modules/accounts/account.route";
import { AuthRoute } from "@src/modules/auth/auth.route";
import { ProductRoute } from "@src/modules/products/product.route";
import { OrderRoute } from "@src/modules/orders/order.route";
import { PaymentRoute } from "@src/modules/payments/payment.route";

export const ApiRoute = Router();

ApiRoute.use("/auth", AuthRoute);
ApiRoute.use("/accounts", AccountRoute);
ApiRoute.use("/products", ProductRoute);
ApiRoute.use("/orders", OrderRoute);
ApiRoute.use("/payments", PaymentRoute);
