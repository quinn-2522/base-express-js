import { Request, Response } from "express";
import { NODE_ENV } from "@src/configs/env";
import { HttpException, HttpStatus } from "@src/exceptions/http.exception";
import { Logger } from "@src/utils/logger.util";

export class ErrorHandler {

  public static notFound(req: Request, res: Response) {
    res.status(404).json({ message: "Page not found" });
  };


  public static error(err: HttpException | any, req: Request, res: Response, next) {
    let error = err;
    if (error && !(error instanceof HttpException)) {
      const statusCode = HttpStatus.INTERNAL_SERVER_ERROR;
      const message = "An error occurred";
      error = new HttpException(message, statusCode, false, err.stack);
    }
    let { statusCode, message } = error;
    const response = {
      code: statusCode,
      message
    };

    if (NODE_ENV) {
      Logger.error(err);
    }

    res.status(statusCode).send(response);
  }

}
