export const accountRef = (select?: string) => ({
  path: "account",
  model: "Account",
  select: select || "displayName avatar"
})