export const productsRef = (select?: string) => ({
  path: "products.product",
  model: "Product",
  select: select || ""
})