export class Logger {
  static info(data: any) {
    console.log("\x1b[32m%s\x1b[0m", data);
  }

  static warn(data: any) {
    console.log("\x1b[33m%s\x1b[0m", data);
  }

  static error(data: any) {
    console.log("\x1b[31m%s\x1b[0m", `[error] `, data);
  }

}
