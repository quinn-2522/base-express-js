import "express-async-errors";
import { Router } from "express";
import { AccountController } from "@src/modules/accounts/account.controller";
import { AuthMiddleware } from "@src/middlewares/auth.middleware";
import { RoleEnum } from "@src/modules/accounts/account.enum";

export const AccountRoute = Router();

AccountRoute
  .use(AuthMiddleware.authenticate())
  .get("/me", AccountController.getSelf)

  .use(AuthMiddleware.authenticate(RoleEnum.ADMIN))
  .get("/", AccountController.findAll);
