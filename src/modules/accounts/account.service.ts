import { BaseService } from "@src/services/base-service";
import { AccountDocument, AccountModel } from "@src/modules/accounts/account.model";

export class AccountService extends BaseService<AccountDocument> {
  constructor() {
    super(AccountModel);
  }
}
