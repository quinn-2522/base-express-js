import mongoose, { Document, PaginateModel } from "mongoose";
import paginate from "mongoose-paginate-v2";
import bcrypt from "bcryptjs";
import { BCRYPT_SALT } from "@src/configs/env";
import {RoleEnum} from "@src/modules/accounts/account.enum";

export interface AccountDocument extends Account, Document {
}

const AccountSchema = new mongoose.Schema<AccountDocument>({
  username: {
    type: String,
    trim: true,
    required: true,
    unique: true
  },
  avatar: {
    type: String,
    default: "https://i.imgur.com/Uoeie1w.jpg"
  },
  displayName: {
    type: String,
    trim: true
  },
  email: {
    type: String,
    trim: true,
    required: true,
    unique: true
  },
  role: {
    type: String,
    required: true,
    enum: RoleEnum,
    default: RoleEnum.USER
  },
  password: {
    type: String,
    select: false,
    required: true,
    trim: true
  }
}, {
  timestamps: true
});

AccountSchema.plugin(paginate);

AccountSchema.pre("save", async function(next) {
  const doc = this;
  if (!doc.displayName) doc.displayName = doc.username;
  if (doc.isModified("password")) doc.password = bcrypt.hashSync(doc.password, BCRYPT_SALT);
  next();
});

AccountSchema.pre("findOneAndUpdate", async function(next) {
  const doc = this;
  if (doc["_update"] && doc["_update"]["password"]) doc["_update"]["password"] = bcrypt.hashSync(doc["_update"]["password"], BCRYPT_SALT);
  next();
});


export const AccountModel = mongoose.model<AccountDocument, PaginateModel<AccountDocument>>("Account", AccountSchema);
