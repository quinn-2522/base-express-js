import { Request, Response } from "express";
import { AccountService } from "./account.service";

interface AccountFilter {
  username?: string;
}

export class AccountController {

  static accountService = new AccountService();

  static async findAll(req: Request, res: Response) {
    const filter = ((keys: AccountFilter) => (keys))(req.query);
    const options = ((keys: PaginateOptions) => (keys))(req.query);

    const data = await AccountController.accountService.findAll(filter, options);
    res.json(data);
  }

  static async getSelf(req: Request, res: Response) {
    res.json({ data: req.account });
  }
}


