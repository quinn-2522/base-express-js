import { BaseService } from "@src/services/base-service";
import {ProductDocument, ProductModel} from "@src/modules/products/product.model";

export class ProductService extends BaseService<ProductDocument> {
  constructor() {
    super(ProductModel);
  }
}
