import Joi from "joi";

export const AddProductValidation = {
  body: Joi.object({
    name: Joi.string().required(),
    image: Joi.string().required(),
    description: Joi.string().required(),
    price: Joi.string().required(),
  })
};

export const UpdateProductValidation = {
  body: Joi.object({
    name: Joi.string(),
    image: Joi.string(),
    price: Joi.string(),
    description: Joi.string()
  })
};