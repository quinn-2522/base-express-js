import { Request, Response } from "express";
import { ProductService } from "./product.service";

interface ProductFilter {
  name?: string;
}

export class ProductController {

  static productService = new ProductService();

  static async findAll(req: Request, res: Response) {
    const filter = ((keys: ProductFilter) => (keys))(req.query);
    const options = ((keys: PaginateOptions) => (keys))(req.query);

    const data = await ProductController.productService.findAll(filter, options);
    res.json(data);
  }

  static async findOne(req: Request, res: Response) {
    const data = await ProductController.productService.findOne({ _id: req.params.productId });
    res.json({ data });
  }

  static async add(req: Request, res: Response) {

    Object.assign(req.body, { account: req.account._id });

    const data = await ProductController.productService.create(req.body);
    res.json({ data });
  }

  static async update(req: Request, res: Response) {
    const data = await ProductController.productService.update({
      _id: req.params.productId,
      account: req.account._id
    }, req.body, { new: true });
    res.json({ data, message: "Removed order successfully" });
  }

  static async delete(req: Request, res: Response) {
    const data = await ProductController.productService.remove({ _id: req.params.productId, account: req.account._id });
    res.json({ message: "Removed order successfully" });
  }

}


