import mongoose, { Document, PaginateModel } from "mongoose";
import paginate from "mongoose-paginate-v2";

export interface ProductDocument extends Product, Document {
}

const ProductSchema = new mongoose.Schema<ProductDocument>({
  name: {
    type: String,
    trim: true,
    required: true,
  },
  account: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account",
    required: true,
  },
  image: {
    type: String,
    default: "https://i.imgur.com/Uoeie1w.jpg"
  },
  price: {
    type: String,
    trim: true,
    default: "0"
  },
  description: {
    type: String,
    trim: true,
  }
}, {
  timestamps: true
});

ProductSchema.plugin(paginate);

export const ProductModel = mongoose.model<ProductDocument, PaginateModel<ProductDocument>>("Product", ProductSchema);
