import "express-async-errors";
import { Router } from "express";
import { ProductController } from "@src/modules/products/product.controller";
import { AuthMiddleware } from "@src/middlewares/auth.middleware";
import { ValidationMiddleware } from "@src/middlewares/validation.middleware";
import { AddProductValidation, UpdateProductValidation } from "@src/modules/products/product.validation";
import { RoleEnum } from "@src/modules/accounts/account.enum";

export const ProductRoute = Router();

ProductRoute
  .get("/",
    ProductController.findAll
  )
  .get("/:productId",
    ProductController.findOne
  )
  .use(AuthMiddleware.authenticate(RoleEnum.ADMIN))
  .post("/",
    ValidationMiddleware.validate(AddProductValidation),
    ProductController.add
  )
  .patch("/:productId",
    ValidationMiddleware.validate(UpdateProductValidation),
    ProductController.update
  )
  .delete("/:productId", ProductController.delete);
