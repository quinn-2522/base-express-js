import Joi from "joi";

export const AddPaymentValidation = {
  body: Joi.object({
    paymentInfo: Joi.string().required()
  })
};
