import "express-async-errors";
import { Router } from "express";
import { AuthMiddleware } from "@src/middlewares/auth.middleware";
import { ValidationMiddleware } from "@src/middlewares/validation.middleware";
import { AddPaymentValidation } from "@src/modules/payments/payment.validation";
import { PaymentController } from "@src/modules/payments/payment.controller";

export const PaymentRoute = Router();

PaymentRoute
  .use(AuthMiddleware.authenticate())
  .post("/checkout",
    // ValidationMiddleware.validate(AddPaymentValidation),
    PaymentController.addPayment
  )
