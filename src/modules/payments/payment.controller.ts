import { Request, Response } from "express";
const stripe = require("stripe")("sk_test_51MfwEdKohQReWAQDX0VeL6ZTPQNG3SR6UsYqj3MuYdpbEBlDX5Sdotquir3rMhR9K45lElP2cxrhVN4ywUTfOjEB00XfrTxIQi");
import {AES, Rabbit, RC4, RC4Drop, enc} from 'crypto-js';
import { HttpException } from "@src/exceptions/http.exception";
import httpStatus from "http-status";

export type EncAlgorithm = 'AES' | 'Rabbit' | 'RC4' | 'RC4Drop';
export type Encryptation = {
  encrypt(value: string): string;
  decrypt(value: string): string;
};

const algorithms = {
  AES,
  Rabbit,
  RC4,
  RC4Drop,
};

export function getEncryption(
  encAlgorithm: EncAlgorithm,
  secretKey: string,
): Encryptation {
  return {
    encrypt: (value: string): string => {

      return algorithms[encAlgorithm].encrypt(value, secretKey).toString();
    },
    decrypt: (value: string): string => {
      return algorithms[encAlgorithm]
        .decrypt(value, secretKey)
        .toString(enc.Utf8);
    },
  };
}

export class PaymentController {

  static async addPayment(req: Request, res: Response) {
    const key = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCsGsaUx0GV6Eys99mvIX5li8dyyNc+X2MNdYCMiZzyBjAgL03Wu5upjErLID5OpoylCupBKHJ+NMd6Wlw0pkWnNUZ1KGXBw+ssryDlUApBFZCV0lGIXO3vg8dXqbjSUe1t5N+QcgzDiLHRxl5bnM0VUQUqI3cTSHNBxcQc/XuyfwIDAQAB"
    const decodedBody = getEncryption('RC4', key).decrypt(req.body.paymentInfo)

    if (!decodedBody) throw new HttpException("Bad request", httpStatus.BAD_REQUEST)

    const { amount, source, receipt_email } = JSON.parse(decodedBody);
    try {
      const charge = await stripe.charges.create({
        amount: amount,
        currency: "usd",
        source,
        receipt_email,
      });
      console.log(charge);
    } catch (err) {
      console.log(err);
    }

    res.send("test");
  }

}


