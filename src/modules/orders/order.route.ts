import "express-async-errors";
import { Router } from "express";
import { OrderController } from "@src/modules/orders/order.controller";
import { AuthMiddleware } from "@src/middlewares/auth.middleware";
import { ValidationMiddleware } from "@src/middlewares/validation.middleware";
import { AddOrderValidation, UpdateOrderValidation } from "@src/modules/orders/order.validation";
import { RoleEnum } from "@src/modules/accounts/account.enum";

export const OrderRoute = Router();

OrderRoute
  .use(AuthMiddleware.authenticate())
  .post("/",
    ValidationMiddleware.validate(AddOrderValidation),
    OrderController.add
  )
  .get("/",
    OrderController.findSelfOrders
  )
  .get("/:orderId",
    OrderController.findOne
  )
  .patch("/:orderId",
    ValidationMiddleware.validate(UpdateOrderValidation),
    OrderController.update
  )
  .delete("/:orderId", OrderController.delete)
  .use(AuthMiddleware.authenticate(RoleEnum.ADMIN))
  .get("/",
    OrderController.findAll
  );