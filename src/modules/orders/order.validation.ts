import Joi from "joi";

export const AddOrderValidation = {
  body: Joi.object({
    products: Joi.array().items(Joi.object({
      product: Joi.string(),
      quantity: Joi.number()
    })).required()
  })
};

export const UpdateOrderValidation = {
  body: Joi.object({
    products: Joi.array().items(Joi.object({
      product: Joi.string(),
      quantity: Joi.number()
    }))
  })
};