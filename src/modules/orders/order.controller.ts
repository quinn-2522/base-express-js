import { Request, Response } from "express";
import { OrderService } from "./order.service";
import { accountRef } from "@src/enums/populate-ref/account.ref";
import { productsRef } from "@src/enums/populate-ref/product.ref";
import { ProductService } from "@src/modules/products/product.service";
import { HttpException, HttpStatus } from "@src/exceptions/http.exception";

interface OrderFilter {
  account?: string;
}

export class OrderController {

  static productService = new ProductService();
  static orderService = new OrderService();

  static async findAll(req: Request, res: Response) {
    const filter = ((keys: OrderFilter) => (keys))(req.query);
    const options = ((keys: PaginateOptions) => (keys))(req.query);

    Object.assign(options, { populate: [accountRef(), productsRef()] });

    const data = await OrderController.orderService.findAll(filter, options);
    res.json(data);
  }


  static async findSelfOrders(req: Request, res: Response) {
    const filter = ((keys: OrderFilter) => (keys))(req.query);
    const options = ((keys: PaginateOptions) => (keys))(req.query);

    Object.assign(filter, { account: req.account._id });
    Object.assign(options, { populate: [accountRef(), productsRef()] });

    const data = await OrderController.orderService.findAll(filter, options);
    res.json(data);
  }

  static async findOne(req: Request, res: Response) {
    const data = await OrderController.orderService.findOne({ _id: req.params.orderId, account: req.account._id });
    res.json({ data });
  }

  static async add(req: Request, res: Response) {

    Object.assign(req.body, { account: req.account._id });

    const existsProducts = (await OrderController.productService.findAll({}, { lean: true })).data.map(data => data._id.toString());

    for (const product of req.body.products) {
      if (!existsProducts.includes(product.product.toString())) throw new HttpException(`Product with id ${product.product} does not exists`, HttpStatus.BAD_REQUEST);
    }

    const data = await OrderController.orderService.create(req.body);
    res.json({ data });
  }

  static async update(req: Request, res: Response) {
    const data = await OrderController.orderService.update({
      _id: req.params.orderId,
      account: req.account._id
    }, req.body, { new: true });
    res.json({ data, message: "Removed order successfully" });
  }

  static async delete(req: Request, res: Response) {
    const data = await OrderController.orderService.remove({ _id: req.params.orderId, account: req.account._id });
    res.json({ message: "Removed order successfully" });
  }

}


