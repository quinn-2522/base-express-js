import mongoose, { Document, PaginateModel } from "mongoose";
import paginate from "mongoose-paginate-v2";

export interface OrderDocument extends Order, Document {
}

const OrderSchema = new mongoose.Schema<OrderDocument>({
  account: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account",
    required: true,
  },
  products: [{
    product: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Order",
    },
    quantity: {
      type: Number,
      default: 1
    }
  }],
}, {
  timestamps: true
});

OrderSchema.plugin(paginate);

export const OrderModel = mongoose.model<OrderDocument, PaginateModel<OrderDocument>>("Order", OrderSchema);
