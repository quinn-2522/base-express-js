import { BaseService } from "@src/services/base-service";
import {OrderDocument, OrderModel} from "@src/modules/orders/order.model";

export class OrderService extends BaseService<OrderDocument> {
  constructor() {
    super(OrderModel);
  }
}
