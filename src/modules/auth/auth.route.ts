import "express-async-errors";
import { Router } from "express";
import { AuthController } from "@src/modules/auth/auth.controller";
import { ValidationMiddleware } from "@src/middlewares/validation.middleware";
import {
  LoginValidation,
  LogoutValidation,
  RefreshTokenValidation,
  RegisterAccountValidation
} from "@src/modules/auth/auth.validation";
import { AuthMiddleware } from "@src/middlewares/auth.middleware";

export const AuthRoute = Router();

AuthRoute
  .post("/login",
    ValidationMiddleware.validate(LoginValidation),
    AuthController.login
  )
  .post("/register",
    ValidationMiddleware.validate(RegisterAccountValidation),
    AuthController.register
  )
  .post("/refresh-token",
    ValidationMiddleware.validate(RefreshTokenValidation),
    AuthController.refreshToken
  )
  .post("/logout",
    AuthMiddleware.authenticate(),
    ValidationMiddleware.validate(LogoutValidation),
    AuthController.logout
  );
