import Joi from "joi";

export const LoginValidation = {
  body: Joi.object({
    loginField: Joi.string().required(),
    password: Joi.string().required()
  })
};

export const RegisterAccountValidation = {
  body: Joi.object({
    email: Joi.string().required(),
    username: Joi.string().required(),
    password: Joi.string().required()
  })
};

export const RefreshTokenValidation = {
  body: Joi.object({
    refreshToken: Joi.string().required()
  })
};

export const LogoutValidation = {
  body: Joi.object({
    refreshToken: Joi.string().required()
  })
};
