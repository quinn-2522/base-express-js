import { Request, Response } from "express";
import { AuthService } from "@src/modules/auth/auth.service";
import { TokensService } from "@src/modules/tokens/tokens.service";

export class AuthController {

  static authService = new AuthService();
  static tokensService = new TokensService();

  static async register(req: Request, res: Response) {
    await AuthController.authService.register(req.body);

    res.json({ message: "Register account successfully" });
  }

  static async login(req: Request, res: Response) {
    const account = await AuthController.authService.authenticate(req.body.loginField, req.body.password);

    const payload = {
      accountId: account._id
    };

    let {
      access_token,
      refresh_token,
      accessTokenExpiresIn
    } = await AuthController.authService.generateAuthTokens(payload, account);

    res.json({
      access_token,
      access_token_expires_in: accessTokenExpiresIn,
      refresh_token,
      account
    });
  }

  static async refreshToken(req: Request, res: Response) {
    const tokens = await AuthController.authService.refreshToken(req.body.refreshToken);

    return res.json({
      access_token: tokens.accessToken,
      access_token_expires_in: tokens.accessTokenExpiresIn,
      refresh_token: tokens.refreshToken,
      account: tokens.account
    });

  }

  static async logout(req: Request, res: Response) {
    await AuthController.tokensService.remove({
      author: req.account._id,
      token: req.body.refreshToken
    });

    res.json("Logged out successfully");
  }
}


