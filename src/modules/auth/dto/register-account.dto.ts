import Joi from "joi";

export class RegisterAccountDto {
  username: string;
  email: string;
  password: string;
}

