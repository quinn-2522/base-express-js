import Joi from "joi";

export class LoginDto {
  loginField: string;
  password: string;
}