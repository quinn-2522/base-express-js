import { AccountService } from "@src/modules/accounts/account.service";
import { TokensService } from "@src/modules/tokens/tokens.service";
import { HttpException, HttpStatus } from "@src/exceptions/http.exception";
import { RegisterAccountDto } from "@src/modules/auth/dto/register-account.dto";
import { compareSync } from "bcryptjs";
import { TokenTypeEnum, TokenTypes } from "@src/modules/tokens/token.enum";
import jwt from "jsonwebtoken";
import {
  ACCESS_TOKEN_EXPIRES_TIME,
  ACCESS_TOKEN_SECRET,
  REFRESH_TOKEN_EXPIRES_TIME,
  REFRESH_TOKEN_SECRET,
  RESET_PASS_TOKEN_EXPIRES_TIME,
  RESET_PASS_TOKEN_SECRET,
  VERIFY_EMAIL_TOKEN_EXPIRES_TIME,
  VERIFY_EMAIL_TOKEN_SECRET
} from "@src/configs/env";
import moment from "moment";
import { AccountDocument } from "@src/modules/accounts/account.model";

export class AuthService {

  private readonly accountService = new AccountService();
  private readonly tokensService = new TokensService();

  async register(registerAccountDto: RegisterAccountDto) {
    if (!!(await this.accountService.findOne({ $or: [{ username: registerAccountDto.username }, { email: registerAccountDto.email }] }, { nullable: true }))) throw new HttpException("This account is already exists", 500);

    return await this.accountService.create(registerAccountDto);
  }

  async generateAuthTokens(payload: TokenPayload, account: AccountDocument) {

    let {
      accessTokenExpiresIn, refreshTokenExpiresIn
    } = await this.generateTokensExpirationTime();

    const access_token = this.generateToken({
      ...payload,
      type: TokenTypeEnum.ACCESS
    }, TokenTypeEnum.ACCESS, { expiresIn: accessTokenExpiresIn });

    const refresh_token = this.generateToken({
      ...payload,
      type: TokenTypeEnum.REFRESH
    }, TokenTypeEnum.REFRESH, { expiresIn: refreshTokenExpiresIn });

    await this.saveToken(
      refresh_token,
      account,
      TokenTypeEnum.REFRESH,
      refreshTokenExpiresIn
    );

    return {
      access_token,
      refresh_token,
      accessTokenExpiresIn,
      refreshTokenExpiresIn
    };
  }

  async refreshToken(token: string) {
    let refreshToken;
    refreshToken = await this.tokensService.findOne({ token: token }, {
      populate: {
        path: "account",
        model: "Account",
        select: "displayName avatar email username"
      }
    });
    if (!refreshToken) {
      throw new HttpException("Unauthorized", HttpStatus.UNAUTHORIZED);
    }
    const account = refreshToken.account;

    console.log(refreshToken.account);
    const payload = await this.verifyToken(refreshToken.token, TokenTypeEnum.REFRESH);
    const accessTokenExpiresIn = await this.generateTokensExpirationTime(TokenTypeEnum.ACCESS);
    const refreshTokenExpiresIn = await this.generateTokensExpirationTime(TokenTypeEnum.REFRESH);

    const accessToken = this.generateToken({
      accountId: payload.accountId,
      type: TokenTypeEnum.ACCESS
    }, TokenTypeEnum.ACCESS, { expiresIn: accessTokenExpiresIn });

    refreshToken = this.generateToken({
      accountId: payload.accountId,
      type: TokenTypeEnum.ACCESS
    }, TokenTypeEnum.ACCESS, { expiresIn: refreshTokenExpiresIn });

    return { accessToken, refreshToken, accessTokenExpiresIn, account };
  }

  async saveToken(token: string, account: AccountDocument, type: TokenTypeEnum, expiresIn?: number) {
    await this.tokensService.create({
      token,
      account: account._id,
      expiresTime: moment().utcOffset("+0700").add(expiresIn, "s").toDate(),
      type: type
    });
  }

  async authenticate(loginField: string, password: string) {
    let account = await this.accountService.findOne({ $or: [{ email: loginField }, { username: loginField }] }, {
      select: "displayName password avatar email username"
    });

    const check = await this.comparePassword(password, account.password);
    if (!account || !check) {
      throw new HttpException("Failed to authenticate", HttpStatus.UNAUTHORIZED);
    }

    delete account["_doc"].password;
    return account;
  }

  async comparePassword(password: string, storePasswordHash: string): Promise<boolean> {
    return compareSync(password, storePasswordHash);
  }

  generateToken(payload: TokenPayload, keyName: TokenTypeEnum, options?: jwt.SignOptions) {
    const privateKeyBase64 = this.getTokenSecret(keyName);
    // const privateKey = Buffer.from(privateKeyBase64, "base64").toString("ascii");
    return jwt.sign(payload, privateKeyBase64, {
      algorithm: "HS256",
      ...options
    });
  }

  getTokenSecret(keyName: TokenTypeEnum): string {
    switch (keyName) {
      case TokenTypeEnum.ACCESS:
        return ACCESS_TOKEN_SECRET;

      case TokenTypeEnum.REFRESH:
        return REFRESH_TOKEN_SECRET;

      case TokenTypeEnum.VERIFY_EMAIL:
        return VERIFY_EMAIL_TOKEN_SECRET;

      case TokenTypeEnum.RESET_PASS:
        return RESET_PASS_TOKEN_SECRET;
    }
  }

  async verifyTokenFromRequest(token: string): Promise<AccountDocument> {
    let payload;
    try {
      payload = this.verifyToken(token, TokenTypeEnum.ACCESS);
    } catch (e) {
      console.log(e);
      throw new HttpException("Unauthorized", HttpStatus.UNAUTHORIZED);
    }

    const account = await this.accountService.findOne({ _id: payload.accountId }, { nullable: true });

    if (!account) {
      throw new HttpException("Account does not exist", HttpStatus.UNAUTHORIZED);
    }

    return account;

  }

  verifyToken(token: string, keyName: TokenTypeEnum): any {
    try {
      const privateKeyBase64 = this.getTokenSecret(keyName);
      return jwt.verify(token, privateKeyBase64);
    } catch (e) {
      this.tokensService.remove({ token }, { nullable: true }).then(_ => {
      });
      throw e;
    }
  }

  async generateTokensExpirationTime(type?: TokenTypeEnum) {
    const expiresTime = {};

    for (const type of TokenTypes) {
      let tokenExpiresTimeByType;
      switch (type) {
        case TokenTypeEnum.ACCESS:
          tokenExpiresTimeByType = ACCESS_TOKEN_EXPIRES_TIME;
          break;

        case TokenTypeEnum.REFRESH:
          tokenExpiresTimeByType = REFRESH_TOKEN_EXPIRES_TIME;
          break;

        case TokenTypeEnum.VERIFY_EMAIL:
          tokenExpiresTimeByType = VERIFY_EMAIL_TOKEN_EXPIRES_TIME;
          break;

        case TokenTypeEnum.RESET_PASS:
          tokenExpiresTimeByType = RESET_PASS_TOKEN_EXPIRES_TIME;
          break;
      }

      if (tokenExpiresTimeByType) {
        expiresTime[type] = moment.duration({
          [tokenExpiresTimeByType.replace(/[^A-Za-z]/g, "")]: parseInt(tokenExpiresTimeByType.match(/\d+/)[0])
        }).asSeconds();
      }
    }

    return (type) ? expiresTime[type] : {
      accessTokenExpiresIn: expiresTime[TokenTypeEnum.ACCESS],
      refreshTokenExpiresIn: expiresTime[TokenTypeEnum.REFRESH],
      emailVerifyTokenExpiresIn: expiresTime[TokenTypeEnum.VERIFY_EMAIL],
      changePasswordTokenExpiresIn: expiresTime[TokenTypeEnum.RESET_PASS],
      success: true
    };
  }

}