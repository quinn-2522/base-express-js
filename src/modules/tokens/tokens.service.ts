import {BaseService} from "@src/services/base-service";
import {TokenDocument, TokensModel} from "@src/modules/tokens/tokens.model";

export class TokensService extends BaseService<TokenDocument> {
  constructor() {
    super(TokensModel);
  }
}
