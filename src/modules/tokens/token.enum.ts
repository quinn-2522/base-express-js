export enum TokenTypeEnum {
  REFRESH = "refresh",
  ACCESS = "access",
  VERIFY_EMAIL = "verify_email",
  RESET_PASS = "reset_pass",
}

export const TokenTypes = Object.values(TokenTypeEnum);