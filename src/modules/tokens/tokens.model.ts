import mongoose, { Document, Schema, model, PaginateModel } from "mongoose";
import pagination from "mongoose-paginate-v2";


export interface TokenDocument extends Token, Document {
}

const TokenSchema = new Schema<TokenDocument>({
  token: {
    type: String,
    required: true
  },
  account: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account",
    required: true
  },
  type: {
    type: String,
    required: true
  },
  expiresTime: {
    type: Date,
    required: true
  }
});

TokenSchema.plugin(pagination);

export const TokensModel = model<TokenDocument, PaginateModel<TokenDocument>>("Token", TokenSchema);
