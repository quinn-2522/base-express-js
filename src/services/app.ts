import {Express} from "@src/services/express"
import {Database} from "@src/services/database";

export class App {
  public static init() {
    this.loadDatabase()
    this.loadServer();
  }

  private static loadServer() {
    Express.init();
  }

  private static loadDatabase() {
    Database.init();
  }
}
