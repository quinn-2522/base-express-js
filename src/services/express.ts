import express from "express";
import { PORT } from "@src/configs/env";
import { ErrorHandler } from "@src/exceptions/error-handler.exception";
import { Logger } from "@src/utils/logger.util";
import compression from "compression";
import helmet from "helmet";
import { BaseRouter } from "@src/routes";
import * as bodyParser from "body-parser";
import cors from "cors";

class ExpressService {
  public express: express.Application;

  constructor() {
    this.express = express();
    this.mountMiddlewares();
    this.mountRoutes();
  }

  private mountMiddlewares() {
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: true }));
    this.express.use(express.json());
    this.express.use(express.urlencoded({ extended: true }));
    this.express.use(compression());
    this.express.use(helmet());
    this.express.use(cors({
      origin: "*"
    }));
  }

  private mountRoutes() {
    this.express.use(BaseRouter);
  }

  public init() {
    this.express.use(ErrorHandler.error);
    this.express.use("*", ErrorHandler.notFound);
    this.express
      .listen(PORT, () => {
        return Logger.info(`[server] Running at 'http://localhost:${PORT}'`);
      })
      .on("error", (err) => {

        return Logger.error(err.message);
      });
  }
}

export const Express = new ExpressService();
