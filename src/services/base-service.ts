import {
  Document,
  FilterQuery,
  HydratedDocument,
  PaginateModel,
  PaginateOptions,
  QueryOptions,
  QueryWithHelpers,
  UnpackedIntersection,
  UpdateQuery
} from "mongoose";
import { HttpException, HttpStatus } from "@src/exceptions/http.exception";

export class BaseService<T extends Document> {
  constructor(private model: PaginateModel<T>) {
  }

  public async create(createDocumentDto: any, options?: QueryOptions): Promise<HydratedDocument<T, {}, {}>> {
    if (options?.uniqueFields) {
      for (const field of options.uniqueFields) {
        if (Object.keys(createDocumentDto).includes(field) && (await this.count({ [field]: createDocumentDto[field] }) > 0)) {
          throw new HttpException(`This ${field} is exists`, HttpStatus.BAD_REQUEST);
        }
      }
    }

    const document = await this.model.create(createDocumentDto);
    if (options?.populate) await document.populate(options?.populate);
    return document;
  }

  public async findAll(filter: FilterQuery<T>, options?: PaginateOptions): Promise<PaginatedDocumentsResponse<T>> {
    const paginateResult = await this.model.paginate(filter, options);
    const data = paginateResult.docs;
    delete paginateResult.docs;
    return {
      data,
      paginationOptions: paginateResult
    };
  }

  public async findOne(filter: FilterQuery<T>, options?: QueryOptions): Promise<UnpackedIntersection<HydratedDocument<T, {}, {}>, {}>> {
    const document = await this.model.findOne(filter, null, options).populate(options?.populate).select(options?.select);
    if (options?.nullable !== true && !document) throw new HttpException(`${this.model.modelName} not found`, HttpStatus.NOT_FOUND);

    return document;
  }

  public async update(filter: FilterQuery<T>, updateDocumentDto: UpdateQuery<T>, options?: QueryOptions): Promise<UnpackedIntersection<HydratedDocument<T, {}, {}>, {}>> {
    const document = await this.model.findOneAndUpdate(filter, updateDocumentDto, options).populate(options?.populate);
    if (options?.nullable !== true && !document) throw new HttpException(`${this.model.modelName} not found`, HttpStatus.NOT_FOUND);

    return document;
  }

  public async remove(filter: FilterQuery<T>, options?: QueryOptions): Promise<UnpackedIntersection<HydratedDocument<T, {}, {}>, {}>> {
    const document = await this.model.findOneAndDelete(filter, options).populate(options?.populate);
    if (options?.nullable !== true && !document) throw new HttpException(`${this.model.modelName} not found`, HttpStatus.NOT_FOUND);

    return document;
  }


  public async removeAll(filter: FilterQuery<T>, options?: QueryOptions) {
    return this.model.deleteMany(filter, options).populate(options?.populate);
  }

  public count(filter: FilterQuery<T>): QueryWithHelpers<number, HydratedDocument<T, {}, {}>, {}, T> {
    return this.model.countDocuments(filter);
  }
}
