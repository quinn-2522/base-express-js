import mongoose from "mongoose";
import {
  DEFAULT_ACCOUNT_DISPLAY_NAME,
  DEFAULT_ACCOUNT_EMAIL, DEFAULT_ACCOUNT_PASSWORD,
  DEFAULT_ACCOUNT_USERNAME,
  MONGO_URL
} from "@src/configs/env";
import { Logger } from "@src/utils/logger.util";
import { AccountModel } from "@src/modules/accounts/account.model";

export class Database {

  public static init() {
    mongoose.set("strictQuery", true);
    mongoose.connect(MONGO_URL, {}, async (error) => {
      if (error) {
        Logger.error("[error] Failed to connect to the Mongo server!!");
        console.log(error);
        throw error;
      } else {
        Logger.info("[database] Connected to mongo server");
        await this.generateInitialData();
      }
    });
  }

  static async generateInitialData() {
    if (!(await AccountModel.findOne({}))) {
      await AccountModel.create({
        username: DEFAULT_ACCOUNT_USERNAME,
        displayName: DEFAULT_ACCOUNT_DISPLAY_NAME,
        role: "admin",
        email: DEFAULT_ACCOUNT_EMAIL,
        password: DEFAULT_ACCOUNT_PASSWORD
      });
    }
  }

}
