import * as dotenv from "dotenv";

dotenv.config();

// environment
export const NODE_ENV: string = process.env.NODE_ENV || "development";

// application
export const DOMAIN: string = process.env.DOMAIN || "localhost";
export const PORT: number = parseInt(process.env.PORT as string) || 3000;
export const END_POINT: string = process.env.END_POINT || "graphql";
export const FE_URL: string = process.env.FE_URL || "xxx";
export const RATE_LIMIT_MAX: number = parseInt(process.env.RATE_LIMIT_MAX as string) || 10000;

// static
export const STATIC: string = process.env.STATIC || "static";

// mongodb
export const MONGO_URL = process.env.MONGO_URL || "mongodb://localhost/ns-debtors";

// jsonwebtoken
export const ACCESS_TOKEN_SECRET: string = process.env.ACCESS_TOKEN_SECRET || "access-token-key";
export const ACCESS_TOKEN_EXPIRES_TIME: string = process.env.ACCESS_TOKEN_EXPIRES_TIME || "60m";
export const REFRESH_TOKEN_SECRET: string = process.env.REFRESH_TOKEN_SECRET || "refresh-token-key";
export const REFRESH_TOKEN_EXPIRES_TIME: string = process.env.REFRESH_TOKEN_EXPIRES_TIME || "14d";
export const VERIFY_EMAIL_TOKEN_SECRET: string = process.env.VERIFY_EMAIL_TOKEN_SECRET || "email-token-key";
export const VERIFY_EMAIL_TOKEN_EXPIRES_TIME: string = process.env.VERIFY_EMAIL_TOKEN_EXPIRES_TIME || "1d";
export const RESET_PASS_TOKEN_SECRET: string = process.env.RESETPASS_TOKEN_SECRET || "resetpass-token-key";
export const RESET_PASS_TOKEN_EXPIRES_TIME: string = process.env.RESET_PASS_TOKEN_EXPIRES_TIME || "10m";

// bcrypt
export const BCRYPT_SALT: number = parseInt(process.env.BCRYPT_SALT as string) || 10;

// default account
export const DEFAULT_ACCOUNT_USERNAME = process.env.DEFAULT_ACCOUNT_USERNAME || "admin"
export const DEFAULT_ACCOUNT_PASSWORD = process.env.DEFAULT_ACCOUNT_PASSWORD || "admin@123"
export const DEFAULT_ACCOUNT_DISPLAY_NAME = process.env.DEFAULT_ACCOUNT_DISPLAY_NAME || "admin"
export const DEFAULT_ACCOUNT_EMAIL = process.env.DEFAULT_ACCOUNT_EMAIL || "admin@gmail.com"
