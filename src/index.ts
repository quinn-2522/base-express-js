import moduleAlias from 'module-alias';
moduleAlias.addAliases({
  "@src": `${__dirname}`,
  "@src/*": `${__dirname}/*`,
});

import { App } from "@src/services/app";


const main = async () => {
  App.init();
}

main().then(_ => {
});
