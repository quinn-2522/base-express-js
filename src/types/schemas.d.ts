interface Account {
  username: string;
  avatar: string;
  password: string;
  displayName?: string;
  email: string;
  role: import("@src/modules/accounts/account.enum").RoleEnum
}

interface Product {
  name: string;
  image: string;
  description: string;
  price: string;
  account: import("@src/modules/accounts/account.model").AccountDocument;
}

interface Order {
  account: import("@src/modules/accounts/account.model").AccountDocument;
  products: [{
    product: import("@src/modules/products/product.model").ProductDocument;
    quantity: string;
  }]
}

interface Token {
  token: string;
  account: import("@src/modules/accounts/account.model").AccountDocument;
  expiresTime: Date;
  type: import("@src/modules/tokens/token.enum").TokenTypeEnum;
}
