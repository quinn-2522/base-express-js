declare module "mongoose" {
  interface QueryOptions {
    select?: string;
    populate?: PopulateOptions | (PopulateOptions | string)[];
    nullable?: boolean;
    uniqueFields?: any[];
  }
}

declare namespace Express {
  export interface Request {
    account?: import("@src/modules/accounts/account.model").AccountDocument;
  }
}

interface PaginatedDocumentsResponse<T> {
  data: T[];
  paginationOptions: Partial<import ("mongoose").PaginateResult<T>>;
}

interface PaginateOptions {
  limit?: number,
  page?: number,
  sort?: string
}

interface TokenPayload {
  accountId: string;
  type?: import("@modules/tokens/token.enum").TokenType;
}
