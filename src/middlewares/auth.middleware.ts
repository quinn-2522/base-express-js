import { NextFunction, Request, Response } from "express";
import { AuthService } from "@src/modules/auth/auth.service";
import { HttpException, HttpStatus } from "@src/exceptions/http.exception";
import {RoleEnum} from "@src/modules/accounts/account.enum";

export class AuthMiddleware {
  private static readonly authService = new AuthService();

  static authenticate(role?: RoleEnum) {
    return async (req: Request, res: Response, next: NextFunction) => {
      let token = req.headers.authorization;

      if (!token || !token.startsWith("Bearer ")) return next(new HttpException("Unauthorized", HttpStatus.UNAUTHORIZED));

      token = token.split("Bearer ")[1];

      try {
        const account = await this.authService.verifyTokenFromRequest(token);

        if (role && account.role !== role) return next(new HttpException("Forbidden", HttpStatus.FORBIDDEN));

        req.account = account;
        next();

      } catch (e) {
        throw e;
      }
    };
  }
}