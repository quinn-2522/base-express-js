import { NextFunction, Request, Response } from "express";
import Joi from "joi";
import { HttpException, HttpStatus } from "@src/exceptions/http.exception";
import { NODE_ENV } from "@src/configs/env";
import { queryFilter } from "@src/utils/filter";

export class ValidationMiddleware {
  static validate(schema) {
    return (req: Request, res: Response, next: NextFunction) => {
      const validSchema = queryFilter(schema, ["params", "query", "body", "file"]);

      const object = queryFilter(req, Object.keys(validSchema));

      const { value, error } = Joi.compile(validSchema)
        .prefs({ errors: { label: "key" }, abortEarly: false })
        .validate(object);

      if (error) {
        throw new HttpException((NODE_ENV === "deployment") ? error.message : "Validation failed", HttpStatus.BAD_REQUEST);
      }

      Object.assign(req, value);
      next();
    };
  }
}